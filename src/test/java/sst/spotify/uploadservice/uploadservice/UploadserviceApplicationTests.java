package sst.spotify.uploadservice.uploadservice;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={SpringBootApplication.class})
class UploadserviceApplicationTests {

    @Test
    void contextLoads() {
    }

}
