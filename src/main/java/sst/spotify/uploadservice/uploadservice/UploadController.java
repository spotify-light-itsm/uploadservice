package sst.spotify.uploadservice.uploadservice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import javax.servlet.MultipartConfigElement;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/upload")
public class UploadController {


    @Value("${audio-directory}")
    private String AUDIO_DIRECTORY;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofBytes(100000000L));
        factory.setMaxRequestSize(DataSize.ofBytes(100000000L));
        return factory.createMultipartConfig();
    }

    @GetMapping("/testUpload")
    public String asdf() {
        return "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"http://www.thymeleaf.org\">\n" +
                "<body>\n" +
                "\n" +
                "<h1>Spring Boot file upload example</h1>\n" +
                "\n" +
                "<form method=\"POST\" action=\"/upload/songupload\" enctype=\"multipart/form-data\">\n" +
                "    <input type=\"file\" name=\"file\" /><br/><br/>\n" +
                "    <input type=\"submit\" value=\"Submit\" />\n" +
                "</form>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
    }

    @PostMapping("/songupload")
    public String singleFileUpload(@RequestParam("file") MultipartFile file) {
        JSONObject statusJson = new JSONObject();

        if (file.isEmpty()) {
            statusJson.put("status", "data-empty");
            return new ResponseEntity(HttpStatus.BAD_REQUEST).toString();
        }

        try {
            byte[] bytes = file.getBytes();
            File f;
            Path path;
            String randHexString = "";

            do{
                randHexString = getRandomHexString(32);
                path = Paths.get(AUDIO_DIRECTORY + randHexString + ".mp3");
                f = new File(path.toString());
            }while(f.exists());

            Files.write(path, bytes);
            statusJson.put("status", "successful");
            statusJson.put("song_id", randHexString);
            return statusJson.toJSONString();
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST).toString();
        }
    }



    private String getRandomHexString(int numchars){
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while(sb.length() < numchars){
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }
}
