**DOKU für UploadService**
von Malina Klein und Michael Lehenauer


Erledigte Schritte 

1.) Java Projekt erstellt mit Spring Boot und Gradle 

2.) Dockerfile erstellt

3.) Dockerimage erstellt (docker build -t uploadservice:latest .) und 
Java Projekt im Container laufen lassen (docker run --user root -v audiovolume:/app/static/audio -p 8123:8123 --network usermanagementservice-master_default uploadservice:latest)

4.) UploadController erstellt. Testupload möglich unter: http://127.0.0.1:8123/upload/testUpload
Der Uploadcontroller nimmt bei http://127.0.0.1:8123/upload/songupload einen POST parameter entgegen. Dieser Parameter ist die mp3 Datei die hochgeladen werden soll.

5.) .gitlab-ci.yml hinzugefügt 

API-Doku zu finden unter "UploadServiceAPI-Doku.yaml".



**SongUpload - Ablauf**

1) Im Frontend wird eine HTML-Form (Dialog) angezeigt, mit der ein File aus dem lokalen Filesystem ausgewählt werden kann.

2) Das File (.mp3) bzw die Form wird an den UploadService gesendet. Der UploadService generiert einen zufällige 32 stelligen HEX-String und speichert den Song im Audiovolume mit dem HEX-String.

3) Bei erfolgreichem Upload wird ein 200er HTTP-Status zurückgeliefert. Zusätzlich wird im Body ein JSON ausgeliefert welches den Namen (32 stelliger HEX-String) beinhaltet.

4) Der HEX-String wird clientseitig zwischengespeichert und es wird nun eine Maske angezeigt, bei jener die Song-Metainformationen eingegeben werden können. Nach Ausfüllen von der "Song-Metainformationen HTML-Form" wird diese MIT dem 32 stelligen HEX-String an den SongService gesendet.

5) Vom SongService wird bei erfolgreichem Eintrag in die Song-Datenbank ein 200er HTTP-Status zurückgeliefert.


**BEHANDLUNG BEI FEHLERFALL:**
Wenn sie Session unterbrochen wird, während die Metainformationen eingegeben werden, (Song wurde bereits im Audiovolume abgespeichert, aber die Metainformationen wurden noch nicht in der Datenbank vom SongService gespeichert) bleibt das File im Audiovolume bestehen. Als "Garbagecollector" könnte in einem definierten Intervall ein Job ausgeführt werden, der überprüft, welche Songs im audiovolume keinen Eintrag in der Datenbank des SongServices haben und diese dann im audiovolume entfernt.
